
<?php get_header(); ?>

<?php
$banner_image_url  =  get_theme_mod( 'banner_image');
$banner_caption_title  =  esc_html__(get_theme_mod( 'banner_caption_title'));
$banner_caption_description  =  esc_html__(get_theme_mod( 'banner_caption_description'));
$button_text  =  esc_html__(get_theme_mod( 'button_text'));
$button_url  =  esc_url(get_theme_mod( 'button_url'));


$envision_content  =  esc_html__(get_theme_mod( 'envision_content'));


$service_image_one  = get_theme_mod( 'service_image_one');
$service_title_one  =esc_html__(get_theme_mod( 'service_title_one'));
$service_content_one  =esc_html__(get_theme_mod( 'service_content_one'));


$service_image_two  = get_theme_mod( 'service_image_two');
$service_title_two  =esc_html__(get_theme_mod( 'service_title_two'));
$service_content_two  =esc_html__(get_theme_mod( 'service_content_two'));

$service_image_three = get_theme_mod( 'service_image_three');
$service_title_three =esc_html__(get_theme_mod( 'service_title_three'));
$service_content_three  =esc_html__(get_theme_mod( 'service_content_three'));

$service_btn_text =esc_html__(get_theme_mod( 'service_btn_text'));
$service_btn_url  =esc_url(get_theme_mod( 'service_btn_url'));

$support_title =esc_html__(get_theme_mod( 'support_title'));


$support_heading_one = esc_html__(get_theme_mod( 'support_heading_one'));
$support_amount_one = esc_html__(get_theme_mod( 'support_amount_one'));
$support_sub_heading_one  =esc_html__(get_theme_mod( 'support_sub_heading_one'));

$support_heading_two = esc_html__(get_theme_mod( 'support_heading_two'));
$support_amount_two =esc_html__(get_theme_mod( 'support_amount_two'));
$support_sub_heading_two =esc_html__(get_theme_mod( 'support_sub_heading_two'));

$support_heading_three = esc_html__(get_theme_mod( 'support_heading_three'));
$support_amount_three =esc_html__(get_theme_mod( 'support_amount_three'));
$support_sub_heading_three  =esc_html__(get_theme_mod( 'support_sub_heading_three'));

?>
    <!-- banner start -->
    <section class="banner-area" style="background-image: url('<?php echo $banner_image_url ?  $banner_image_url :  get_template_directory_uri().'/img/banner/banner-2.png'; ?>')">
        <div class="container">
            <div class="row align-self-center">
                <div class="col-lg-5 col-md-8 col-sm-12">

                    <div class="banner-caption-box">
                        <?php
                        if($banner_caption_title){
                            echo '<h1>'.$banner_caption_title.'</h1>';
                        }
                        if($banner_caption_description){
                            echo '<p>'.$banner_caption_description.'</p>';
                        }
                        if($button_url){ ?>

                            <a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a>
                      <?php    } ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="left-side-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/banner/banner-3.png')">
            <div class="circle"></div>
        </div>
    </section>
    <!-- banner end -->


<?php
 if($envision_content){?>
     <!-- envision-area start -->
     <section class="envision-area">
         <div class="container ">
             <div class="row align-self-center">
                 <div class="col-lg-6 col-md-8 col-sm-12 m-auto">
                     <h2><?php echo $envision_content; ?></h2>
                 </div>
             </div>
         </div>
     </section>
     <!-- envision-area end -->
<?php } ?>


    <!-- service-area start -->
    <section class="service-area">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="single-service">

                        <?php
                        if($service_image_one){
                            ?>

                        <div class="service-img text-center mb-65">
                            <img src="<?php echo $service_image_one; ?>" alt="">
                        </div>

                        <?php
                            }

                            if($service_title_one){
                                ?>
                                <div class="card">
                                    <div class="card-body">
                                        <h5> <?php echo $service_title_one; ?></h5>
                                        <p><?php  echo $service_content_one ?></p>
                                    </div>
                                    <div class="circle-red"></div>
                                </div>
                        <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-12 ml-auto">
                    <div class="single-service">

                        <?php
                        if($service_image_two){
                            ?>

                            <div class="service-img text-center mb-65">
                                <img src="<?php echo $service_image_two; ?>" alt="">
                            </div>

                            <?php
                        }

                        if($service_title_two){
                            ?>
                            <div class="card">
                                <div class="card-body">
                                    <h5> <?php echo $service_title_two; ?></h5>
                                    <p><?php  echo $service_content_two ?></p>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <div class="single-service">

                        <?php
                        if($service_image_three){
                            ?>

                            <div class="service-img text-center mb-65">
                                <img src="<?php echo $service_image_three; ?>" alt="">
                            </div>

                            <?php
                        }

                        if($service_title_three){
                            ?>
                            <div class="card">
                                <div class="card-body">
                                    <h5> <?php echo $service_title_three; ?></h5>
                                    <p><?php  echo $service_content_three ?></p>
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
                <?php
                if ($service_btn_url){
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="single-service text-center learn-more-btn ">

                            <a href="<?php echo $service_btn_url; ?>" class="btn"><?php echo $service_btn_text; ?></a>

                        </div>
                    </div>
                <?php
                } ?>

            </div>

        </div>
        <div class="circle-blue"></div>
    </section>
    <!-- service-area end -->

    <section class="support-area">
        <div class="container">
            <?php
            if($support_title){
               ?>
                <div class="row mb-100">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <h2><?php echo $support_title; ?></h2>
                    </div>
                </div>
               <?php
            }
            ?>

            <div class="row">

                <?php
                if ($support_heading_one){?>

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="support-box">
                            <h6 class="mb-20"><?php echo $support_heading_one; ?></h6>
                            <h2 class="mb-20"><?php echo  $support_amount_one; ?></h2>
                            <h6 class="m-0"> <?php echo $support_sub_heading_one; ?></h6>
                        </div>

                    </div>

              <?php  }
                ?>

                <?php
                if ($support_heading_two){?>

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="support-box">
                            <h6 class="mb-20"><?php echo $support_heading_two; ?></h6>
                            <h2 class="mb-20"><?php echo  $support_amount_two; ?></h2>
                            <h6 class="m-0"> <?php echo $support_sub_heading_two; ?></h6>
                        </div>

                    </div>

                <?php  }
                ?>

                <?php
                if ($support_heading_three){?>

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="support-box">
                            <h6 class="mb-20"><?php echo $support_heading_three; ?></h6>
                            <h2 class="mb-20"><?php echo  $support_amount_three; ?></h2>
                            <h6 class="m-0"> <?php echo $support_sub_heading_three; ?></h6>
                        </div>

                    </div>

                <?php  }
                ?>

<!--                <div class="col-lg-4 col-md-4 col-sm-6 ">
                    <div class="support-box">
                        <h6 class="mb-20">We’ve Facilitated</h6>
                        <h2 class="mb-20">41,146</h2>
                        <h6 class="m-0"> Miles of Trail Maintenance & Construction</h6>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 ">
                    <div class="support-box">
                        <h6 class="mb-20">Volunteers Have Contributed</h6>
                        <h2 class="mb-20">$108 Million</h2>
                        <h6 class="m-0"> in Volunteer Labor</h6>
                    </div>

                </div>-->

            </div>
        </div>
        <div class="circle-red"></div>
    </section>

    <!-- blog-area start -->
    <section class="blog-area">
        <div class="container">
            <div class="row mb-100">
                <div class="col-12 text-center">
                    <h2 class="heading">Protecting Trails: Current News and Issues</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 pl-20 pr-20">
                    <article>
                        <div class="blog">
                            <div class="card">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog-1.png" class="img-thumbnail img-fill" alt="blog">
                                </div>
                                <div class="card-body">

                                    <h5> 2,802 Miles of Trail to be Improved on International Trails Day</h5>

                                    <div class="content">
                                        <p>
                                            On June 2, 2018, people across the worldwill come together to collectively improve 2,802 miles of trail—the distance across the world—during International  Trails Day. Muncak invites hikers, cyclists,…
                                        </p>
                                    </div>

                                    <a href="">  Read More</a>

                                </div>
                            </div>

                        </div>
                    </article>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 pl-20 pr-20">
                    <article>
                        <div class="blog">
                            <div class="card">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog-2.png" class="img-thumbnail img-fill" alt="blog">
                                </div>
                                <div class="card-body">

                                    <h5>   Four Ways to Celebrate International Trails Day on a Bicycle</h5>

                                    <div class="content">
                                        <p>
                                            There are many ways to celebrate the International Trails Day on June 2. It’s even possible to participate on a bicycle. And if you go on a bike overnight trip during International Trails Dayyou will simultaneously…
                                        </p>
                                    </div>

                                    <a href="">  Read More</a>

                                </div>
                            </div>

                        </div>
                    </article>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 pl-20 pr-20">
                    <article>
                        <div class="blog">
                            <div class="card">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog-3.png" class="img-thumbnail img-fill" alt="blog">
                                </div>
                                <div class="card-body">
                                    <h5>     Millennials are the Future of our Trails</h5>

                                    <div class="content">
                                        <p>
                                            The smell of fresh dew is a welcome scent, as the crunch of my boots melds with the sound of the birds singing their morning song. It’s early and I am wearing layers, but it won’t be long until they are peeled away to adapt to the summer…
                                        </p>
                                    </div>

                                    <a href="">  Read More</a>

                                </div>
                            </div>

                        </div>
                    </article>
                </div>
                <div class="circle-blue"></div>
            </div>
        </div>
    </section>
    <!-- blog-area end -->

 <?php get_footer(); ?>