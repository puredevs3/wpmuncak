(function($, window, document,) {
    "use strict";
    var $win = $(window);
    var $doc = $(document);
    var $body = $('body');

    /*--
    Menu Stick
    -----------------------------------*/




    $body.bind('click', function (e) {
        if ($(e.target).closest('.navbar-toggler').length == 0) {
            var opened = $('.navbar-collapse').hasClass('collapse');

            if (opened === true) {
                $('.navbar-collapse').collapse('hide');
                $(this).find('.icofont-close').addClass('icofont-navigation-menu');
                $(this).find('.icofont-close').removeClass('fa-close');
            }
        }
    });

    $body.on('click', '.navbar-toggler', function (e) {
        e.preventDefault();
        var dd = $(this).attr('aria-expanded');

        if(dd != 'false' ){
            $(this).find('.icofont-close').addClass('icofont-navigation-menu');
            $(this).find('.fa').removeClass('icofont-close');
            $(this).parents('.header-area').find('.navbar-collapse').css({backgroundColor: 'transparent'});
        }else{


            $(this).find('.fa').removeClass('icofont-navigation-menu');
            $(this).find('.fa').addClass('icofont-close');
            $(this).parents('.header-area').find('.navbar-collapse').css({backgroundColor: '#2870db', borderRadius: '20px'});
        }

    });
    
})(jQuery);