<?php
/**
 * Muncak Theme Customizer
 *
 * @package Muncak
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function muncak_customize_register( $wp_customize ) {

    /*  1. Login Button Settings
    /*----------------------------------------*/
    $wp_customize->add_section( 'Login_Button',
        array(
            'title' => __( 'Login Button' ),
            'description' => esc_html__( 'You can easily customise Login Button' ),
            'panel' => '',
            'priority' => 150,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'active_callback' => '',
            'description_hidden' => 'false',
        )
    );
    $wp_customize->add_setting( 'Login_button_text',
        array(
            'default' => 'Login Button Text',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => '',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'Login_button_text',
        array(
            'label' => __( 'Login Button Text' ),
            'description' => esc_html__( 'Login Button Text only text' ),
            'section' => 'Login_Button',
            'priority' => 1,
            'type' => 'text',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'class' => 'my-custom-class',
                'style' => 'border: 1px solid rebeccapurple',
                'placeholder' => __( 'Button text write here' ),
            ),
        )
    );
    $wp_customize->add_setting( 'Login_button_url',
        array(
            'default' => 'Login Button URL',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => 'themeslug_sanitize_url',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'Login_button_url',
        array(
            'label' => __( 'Login Button URL' ),
            'description' => esc_html__( 'Login Button URL only URL' ),
            'section' => 'Login_Button',
            'priority' => 2,
            'type' => 'url',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'placeholder' => __( 'http://www.google.com' ),
            ),
        )
    );


    /*  2. Banner Settings
    /*----------------------------------------*/

    $wp_customize->add_section( 'banner',
        array(
            'title' => __( 'Banner Section' ),
            'description' => esc_html__( 'You can easily Banner Section customise from here' ),
            'panel' => '', // Only needed if adding your Section to a Panel
            'priority' => 160, // Not typically needed. Default is 160
            'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
            'theme_supports' => '', // Rarely needed
            'active_callback' => '', // Rarely needed
            'description_hidden' => 'false', // Rarely needed. Default is False
        )
    );
    $wp_customize->add_setting( 'banner_caption_title',
        array(
            'default' => 'caption title',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => '',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'banner_caption_title',
        array(
            'label' => __( 'Banner Caption Title' ),
            'description' => esc_html__( 'Banner Caption Title only text' ),
            'section' => 'banner',
            'priority' => 1,
            'type' => 'text',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'class' => 'my-custom-class',
                'style' => 'border: 1px solid rebeccapurple',
                'placeholder' => __( 'caption title write here' ),
            ),
        )
    );
    $wp_customize->add_setting( 'banner_caption_description',
        array(
            'default' => 'Caption Description',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => '',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'banner_caption_description',
        array(
            'label' => __( 'Banner Caption Description' ),
            'description' => esc_html__( 'Banner Caption Description only text' ),
            'section' => 'banner',
            'priority' => 2,
            'type' => 'textarea',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'class' => 'my-custom-class',
                'style' => 'border: 1px solid rebeccapurple',
                'placeholder' => __( 'Description write here' ),
            ),
        )
    );
    $wp_customize->add_setting( 'button_text',
        array(
            'default' => 'Button Text',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => '',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'button_text',
        array(
            'label' => __( 'Button Text' ),
            'description' => esc_html__( 'Button Text only text' ),
            'section' => 'banner',
            'priority' => 3,
            'type' => 'text',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'class' => 'my-custom-class',
                'style' => 'border: 1px solid rebeccapurple',
                'placeholder' => __( 'Button text write here' ),
            ),
        )
    );
    $wp_customize->add_setting( 'button_url',
        array(
            'default' => 'Button URL',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => 'themeslug_sanitize_url',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'button_url',
        array(
            'label' => __( 'Button URL' ),
            'description' => esc_html__( 'Button URL only URL' ),
            'section' => 'banner',
            'priority' => 4,
            'type' => 'url',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'placeholder' => __( 'http://www.google.com' ),
            ),
        )
    );
    $wp_customize->add_setting( 'banner_image',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'banner_image',
        array(
            'label' => __( 'Banner Image Upload' ),
            'description' => esc_html__( 'Banner Image Upload From here' ),
            'section' => 'banner',
            'button_labels' => array( // Optional.
                'select' => __( 'Select Image' ),
                'change' => __( 'Change Image' ),
                'remove' => __( 'Remove' ),
                'default' => __( 'Default' ),
                'placeholder' => __( 'No image selected' ),
                'frame_title' => __( 'Select Image' ),
                'frame_button' => __( 'Choose Image' ),
            )
        )
    ) );

    /*  3. Envision  Settings
   /*----------------------------------------*/
    $wp_customize->add_section( 'envision',
        array(
            'title' => __( 'Envision Section' ),
            'description' => esc_html__( 'You can easily Envision Section customise from here' ),
            'panel' => '',
            'priority' => 161,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'active_callback' => '',
            'description_hidden' => 'false',
        )
    );
    $wp_customize->add_setting( 'envision_content',
        array(
            'default' => 'Envision Content',
            'transport' => 'refresh',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'validate_callback' => '',
            'sanitize_callback' => '',
            'sanitize_js_callback' => '',
            'dirty' => false,
        )
    );
    $wp_customize->add_control( 'envision_content',
        array(
            'label' => __( 'Envision Content' ),
            'description' => esc_html__( 'Envision Content only text' ),
            'section' => 'envision',
            'priority' => 1,
            'type' => 'textarea',
            'capability' => 'edit_theme_options',
            'input_attrs' => array(
                'placeholder' => __( 'Envision Title write here' ),
            ),
        )
    );


    /*  4. Service  Settings
/*----------------------------------------*/

    $wp_customize->add_panel( 'service_section', array(
        'priority'       => 250,
        'theme_supports' => '',
        'title'          => __( 'Service Blocks', 'muncak' ),
        'description'    => __( 'Set editable Service Area for certain content.', 'muncak' ),
    ) );

    $wp_customize->add_section( 'block_one' , array(
        'title'    => __('Service Block One','muncak'),
        'panel'    => 'service_section',
        'priority' =>65
    ) );
    $wp_customize->add_setting( 'service_image_one',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_image_one',
        array(
            'label' => __( 'Service Image Upload' ),
            'description' => esc_html__( 'Service Image Upload From here' ),
            'section' => 'block_one',
            'button_labels' => array( // Optional.
                'select' => __( 'Select Image' ),
                'change' => __( 'Change Image' ),
                'remove' => __( 'Remove' ),
                'default' => __( 'Default' ),
                'placeholder' => __( 'No image selected' ),
                'frame_title' => __( 'Select Image' ),
                'frame_button' => __( 'Choose Image' ),
            ),
            'priority' => 1,
        )
    ) );
    $wp_customize->add_setting( 'service_title_one', array(
        'default'           => __( 'default title', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_title_one',
            array(
                'label'    => __( 'Service Title', 'muncak' ),
                'section'  => 'block_one',
                'settings' => 'service_title_one',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'service_content_one', array(
        'default'           => __( 'default Content', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_content_one',
            array(
                'label'    => __( 'Service Content', 'muncak' ),
                'section'  => 'block_one',
                'settings' => 'service_content_one',
                'type' => 'textarea',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );

    $wp_customize->add_section( 'block_two' , array(
        'title'    => __('Service Block Two','muncak'),
        'panel'    => 'service_section',
        'priority' =>66
    ) );
    $wp_customize->add_setting( 'service_image_two',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_image_two',
        array(
            'label' => __( 'Service Image Upload' ),
            'description' => esc_html__( 'Service Image Upload From here' ),
            'section' => 'block_two',
            'button_labels' => array( // Optional.
                'select' => __( 'Select Image' ),
                'change' => __( 'Change Image' ),
                'remove' => __( 'Remove' ),
                'default' => __( 'Default' ),
                'placeholder' => __( 'No image selected' ),
                'frame_title' => __( 'Select Image' ),
                'frame_button' => __( 'Choose Image' ),
            ),
            'priority' => 1,
        )
    ) );
    $wp_customize->add_setting( 'service_title_two', array(
        'default'           => __( 'default title', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_title_two',
            array(
                'label'    => __( 'Service Title', 'muncak' ),
                'section'  => 'block_two',
                'settings' => 'service_title_two',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'service_content_two', array(
        'default'           => __( 'default Content', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_content_two',
            array(
                'label'    => __( 'Service Content', 'muncak' ),
                'section'  => 'block_two',
                'settings' => 'service_content_two',
                'type' => 'textarea',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );


    $wp_customize->add_section( 'block_three' , array(
        'title'    => __('Service Block Three','muncak'),
        'panel'    => 'service_section',
        'priority' =>67
    ) );
    $wp_customize->add_setting( 'service_image_three',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_image_three',
        array(
            'label' => __( 'Service Image Upload' ),
            'description' => esc_html__( 'Service Image Upload From here' ),
            'section' => 'block_three',
            'button_labels' => array( // Optional.
                'select' => __( 'Select Image' ),
                'change' => __( 'Change Image' ),
                'remove' => __( 'Remove' ),
                'default' => __( 'Default' ),
                'placeholder' => __( 'No image selected' ),
                'frame_title' => __( 'Select Image' ),
                'frame_button' => __( 'Choose Image' ),
            ),
            'priority' => 1,
        )
    ) );
    $wp_customize->add_setting( 'service_title_three', array(
        'default'           => __( 'default title', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_title_three',
            array(
                'label'    => __( 'Service Title', 'muncak' ),
                'section'  => 'block_three',
                'settings' => 'service_title_three',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'service_content_three', array(
        'default'           => __( 'default Content', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_content_three',
            array(
                'label'    => __( 'Service Content', 'muncak' ),
                'section'  => 'block_three',
                'settings' => 'service_content_three',
                'type' => 'textarea',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );


    $wp_customize->add_section( 'button_block' , array(
        'title'    => __('Service Button Block','muncak'),
        'panel'    => 'service_section',
        'priority' =>68
    ) );
    $wp_customize->add_setting( 'service_btn_text', array(
        'default'           => __( 'default text', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_btn_text',
            array(
                'label'    => __( 'Service Title', 'muncak' ),
                'section'  => 'button_block',
                'settings' => 'service_btn_text',
                'type'     => 'text',
                'priority' => 1,
            )
        )
    );
    $wp_customize->add_setting( 'service_btn_url', array(
        'default'           => __( 'default Url', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_btn_url',
            array(
                'label'    => __( 'Service Button Url', 'muncak' ),
                'section'  => 'button_block',
                'settings' => 'service_btn_url',
                'type' => 'url',
                'priority' => 2,
                'capability' => 'edit_theme_options'
            )
        )
    );

    /*  5. Support  Settings
   /*----------------------------------------*/
    $wp_customize->add_panel( 'support_section', array(
        'priority'       => 300,
        'theme_supports' => '',
        'title'          => __( 'Supports Blocks', 'muncak' ),
        'description'    => __( 'Set editable Supports Area for certain content.', 'muncak' ),
    ) );

    $wp_customize->add_section( 'support_title_block' , array(
        'title'    => __('Support Title Block','muncak'),
        'panel'    => 'support_section',
        'priority' =>70
    ) );
    $wp_customize->add_setting( 'support_title', array(
        'default'           => __( 'default title', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_title',
            array(
                'label'    => __( 'Support Title', 'muncak' ),
                'section'  => 'support_title_block',
                'settings' => 'support_title',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );




    $wp_customize->add_section( 'support_block_one' , array(
        'title'    => __('Support Block One','muncak'),
        'panel'    => 'support_section',
        'priority' =>70
    ) );

    $wp_customize->add_setting( 'support_heading_one', array(
        'default'           => __( 'default Heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_heading_one',
            array(
                'label'    => __( 'Support Heading', 'muncak' ),
                'section'  => 'support_block_one',
                'settings' => 'support_heading_one',
                'type'     => 'text',
                'priority' =>1,
            )
        )
    );
    $wp_customize->add_setting( 'support_amount_one', array(
        'default'           => __( 'Default AMOUNT', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_amount_one',
            array(
                'label'    => __( 'Support Amount', 'muncak' ),
                'section'  => 'support_block_one',
                'settings' => 'support_amount_one',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'support_sub_heading_one', array(
        'default'           => __( 'default Sub heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_content_one',
            array(
                'label'    => __( 'Support Sub Heading', 'muncak' ),
                'section'  => 'support_block_one',
                'settings' => 'support_sub_heading_one',
                'type' => 'text',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );


    $wp_customize->add_section( 'support_block_two' , array(
        'title'    => __('Support Block Two','muncak'),
        'panel'    => 'support_section',
        'priority' =>75
    ) );

    $wp_customize->add_setting( 'support_heading_two', array(
        'default'           => __( 'default Heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_heading_two',
            array(
                'label'    => __( 'Support Heading', 'muncak' ),
                'section'  => 'support_block_two',
                'settings' => 'support_heading_two',
                'type'     => 'text',
                'priority' =>1,
            )
        )
    );
    $wp_customize->add_setting( 'support_amount_two', array(
        'default'           => __( 'Default AMOUNT', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_amount_two',
            array(
                'label'    => __( 'Support Amount', 'muncak' ),
                'section'  => 'support_block_two',
                'settings' => 'support_amount_two',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'support_sub_heading_two', array(
        'default'           => __( 'default Sub heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_sub_heading_two',
            array(
                'label'    => __( 'Support Sub Heading', 'muncak' ),
                'section'  => 'support_block_two',
                'settings' => 'support_sub_heading_two',
                'type' => 'text',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );



    $wp_customize->add_section( 'support_block_three' , array(
        'title'    => __('Support Block Three','muncak'),
        'panel'    => 'support_section',
        'priority' =>80
    ) );
    $wp_customize->add_setting( 'support_heading_three', array(
        'default'           => __( 'default Heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_heading_three',
            array(
                'label'    => __( 'Support Heading', 'muncak' ),
                'section'  => 'support_block_three',
                'settings' => 'support_heading_three',
                'type'     => 'text',
                'priority' =>1,
            )
        )
    );
    $wp_customize->add_setting( 'support_amount_three', array(
        'default'           => __( 'Default AMOUNT', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'support_amount_three',
            array(
                'label'    => __( 'Support Amount', 'muncak' ),
                'section'  => 'support_block_three',
                'settings' => 'support_amount_three',
                'type'     => 'text',
                'priority' => 2,
            )
        )
    );
    $wp_customize->add_setting( 'support_sub_heading_three', array(
        'default'           => __( 'default Sub heading', 'muncak' ),
        'sanitize_callback' => 'sanitize_text'
    ) );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'service_content_one',
            array(
                'label'    => __( 'Support Sub Heading', 'muncak' ),
                'section'  => 'support_block_three',
                'settings' => 'support_sub_heading_three',
                'type' => 'text',
                'priority' => 3,
                'capability' => 'edit_theme_options'
            )
        )
    );

    // Sanitize text
    function sanitize_text( $text ) {
        return sanitize_text_field( $text );
    }

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'muncak_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'muncak_customize_partial_blogdescription',
		) );
	}

}
add_action( 'customize_register', 'muncak_customize_register' );

function themeslug_sanitize_url( $url ) {
    return esc_url_raw( $url );
}
/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function muncak_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function muncak_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function muncak_customize_preview_js() {
	wp_enqueue_script( 'muncak-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'muncak_customize_preview_js' );


