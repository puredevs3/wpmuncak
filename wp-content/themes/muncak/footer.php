<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Muncak
 */

?>


<!-- footer start -->
<footer class="footer-area">
    <div class="container">
        <div class="news-latter-area">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                    <h2 class="mb-70"> Receive the latest updates on how to protect trails.
                        Subscribe to the monthly edition of Paperless Trails.</h2>

                    <div class="news-latter-form">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control"  placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email address"> </div>
                            <button type="submit" class="btn mt-30">Submit</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="circle-red">

            </div>

        </div>
    </div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h6 class="title"> CONTACT us</h6>
                    <address>Muncak hiking society <br>
                        8605 Second Ave. <br>
                        Silver Spring, MD 20910
                    </address>

                    <ul class="list-unstyled mail-nfo">
                        <li>
                            <a href="mailto:Info@muncak.org"> Info@muncak.org</a>
                        </li>
                        <li>
                            <a href="tell:1-301-565-6704"> 1-301-565-6704 (Main)</a>
                        </li>
                        <li>
                            <a href="tell:1-800-972-8608"> 1-800-972-8608 (Toll-free)</a>
                        </li>
                    </ul>
                </div>
                <div class="col">
                    <h6 class="title"> About us</h6>

                    <ul class="list-unstyled footer-menu">
                        <li>
                            <a href="#">Our Mission</a>
                        </li>
                        <li>
                            <a href="#">Our Team</a>
                        </li>

                        <li>
                            <a href="#">Corporate Sponsors</a>
                        </li>

                        <li>
                            <a href="#">Financials</a>
                        </li>

                        <li>
                            <a href="#">Media Center</a>
                        </li>
                    </ul>
                </div>
                <div class="col">
                    <h6 class="title">Our Programs</h6>
                    <ul class="list-unstyled footer-menu">
                        <li>
                            <a href="#"> Hike The Hill</a>
                        </li>
                        <li>
                            <a href="#"> Volunteer Vacations</a>
                        </li>

                        <li>
                            <a href="#">Alternative Break</a>
                        </li>

                    </ul>
                </div>
                <div class="col">
                    <h6 class="title">  support trails</h6>

                    <ul class="list-unstyled footer-menu">
                        <li>
                            <a href="#"> Donate</a>
                        </li>
                        <li>
                            <a href="#">Individual Membership</a>
                        </li>

                        <li>
                            <a href="#">Gift membership</a>
                        </li>

                        <li>
                            <a href="#">Hiker Store</a>
                        </li>
                    </ul>
                </div>
                <div class="col">
                    <h6 class="title">hiking resources</h6>

                    <ul class="list-unstyled footer-menu">
                        <li>
                            <a href="#">Hikes Near You</a>
                        </li>
                        <li>
                            <a href="#">Hiking 101</a>
                        </li>
                        <li>
                            <a href="#">Muncak Blog</a>
                        </li>
                        <li>
                            <a href="#"> Hiking Organizations</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="list-inline social-link">
                        <li class="list-inline-item">
                            <a href=""><i class="icofont-instagram"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href=""><i class="icofont-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href=""><i class="icofont-twitter"></i></a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

</footer>
<!-- footer end -->

</div>
<!-- /.wrapper start -->

<!-- All JS is here
============================================ -->
<?php wp_footer(); ?>

</body>

</html>