<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Muncak
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<html class="no-js" lang="zxx">
<head>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
        <!-- /.wrapper start -->
        <div class="wrapper">

            <!-- header start -->
            <header class="header-area <?php echo is_home() ? null : 'inner-header'; ?>">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <?php
                        the_custom_logo();
                      /*  if ( is_front_page() && is_home() ) :
                            */?><!--
                            <a class="navbar-brand" href="<?php /*echo esc_url( home_url( '/' ) ); */?>" rel="home"><?php /*bloginfo( 'name' ); */?></a>
                        <?php
                        /*                        else :
                                                    */?>
                            <a class="navbar-brand" href="<?php /*echo esc_url( home_url( '/' ) ); */?>" rel="home"><?php /*bloginfo( 'name' ); */?></a>
                        --><?php
                        /*                        endif;*/
                        $muncak_description = get_bloginfo( 'description', 'display' );
                        if ( $muncak_description || is_customize_preview() ) :
                            ?>
                            <p class="site-description"><?php echo $muncak_description; /* WPCS: xss ok. */ ?></p>
                        <?php endif; ?>

                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa icofont-navigation-menu"></i>
                            <!--<i class="fa icofont-close"></i>-->
                        </button>

                        <?php
                        wp_nav_menu( array(
                                'menu'              => 'primary-menu',
                                'theme_location'    => 'menu-1',
                                'depth'             => 2,
                                'container'         => 'div',
                                'container_id'      => 'navbarSupportedContent',
                                'container_class'   => 'collapse navbar-collapse',
                                'menu_class'        => 'navbar-nav ml-auto',
                                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'            => new WP_Bootstrap_Navwalker())
                        );

                        $button_text  =  esc_html__(get_theme_mod( 'Login_button_text'));
                        $button_url  =  esc_html__(get_theme_mod( 'Login_button_url'));

                        if($button_url){?>
                        <div class="form-inline my-2 my-lg-0 ml-20">
                            <a href="<?php echo $button_url;  ?>" class="login-btn  my-2 my-sm-0"><?php echo $button_text; ?></a>
                        </div>

                        <?php } ?>




                    </nav>
                </div>


            </header>
            <!-- header end -->